/*
 * This file is part of the brainy brainfuck IDE.
 *
 * Copyright (C) 2018   Nicolas Winkler (nicolas.winkler@gmx.ch)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "brainywindow.h"
#include "ui_brainywindow.h"

#include "zombie/zombie.h"
#include "runtime.h"

#include <QFileDialog>
#include <QFontDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QFontMetrics>
#include <iostream>
#include <memory>

BrainyWindow::BrainyWindow(QWidget *parent) :
    QMainWindow{ parent },
    ui{ std::make_unique<Ui::BrainyWindow>() }
{
    ui->setupUi(this);

    // set textboxes to monospace
    setTextFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));

    sh = std::make_unique<SyntaxHighlighter>(ui->textEdit->document());

    // connect signals
    ui->actionOpen->connect(ui->actionOpen, &QAction::triggered, this, &BrainyWindow::openFile);
    ui->actionSave->connect(ui->actionSave, &QAction::triggered, this, &BrainyWindow::saveFile);
    ui->actionSave_As->connect(ui->actionSave_As, &QAction::triggered, this, &BrainyWindow::saveFileAs);
    ui->run->connect(ui->run, &QPushButton::clicked, this, &BrainyWindow::startRun);
    ui->actionEditor_Font->connect(ui->actionEditor_Font, &QAction::triggered, this, [this] () {
        bool ok;
        QFont font = QFontDialog::getFont(&ok, ui->textEdit->font(), this);
        if (ok) {
            setTextFont(font);
        }
    });
}

BrainyWindow::~BrainyWindow()
{
}

void BrainyWindow::setTextFont(const QFont &font)
{
    ui->textEdit->setFont(font);
    ui->runConsole->setFont(font);
    ui->runInput->setFont(font);
}

void BrainyWindow::openFile()
{
    /*auto dialog = std::make_unique<QFileDialog>(this, tr("Open File"), "", tr("Brainfuck File (*.bf);; Any File (*.*)"));
    dialog->show();*/

    auto filename = QFileDialog::getOpenFileName(this,
        tr("Open File"), "", tr("Brainfuck File (*.bf, *.b);; Any File (*.*)"));
    if (filename == "")
        return;

    QFileInfo fileInfo(filename);
    if (!fileInfo.exists()) {
        QMessageBox::critical(this, "Error opening file", "File does not exist");
    }

    openedFile = std::make_unique<QFile>(filename);
    if (!openedFile->open(QIODevice::ReadOnly | QFile::Text)) {
        QMessageBox::critical(this, "Error opening file", "Could not open file");
        return;
    }
    QTextStream in(openedFile.get());
    ui->textEdit->setText(in.readAll());
}


void BrainyWindow::saveFile()
{
    if (!openedFile) {
        auto filename = QFileDialog::getSaveFileName(this,
            tr("Save File"), "", tr("Brainfuck File (*.bf, *.b);; Any File (*.*)"));
        if (filename == "")
            return;
        openedFile = std::make_unique<QFile>(filename);
    }
    if (openedFile) {
        if (openedFile->isOpen() && !openedFile->isWritable()) {
            openedFile->close();
        }
        if (!openedFile->isOpen()) {
            openedFile->open(QIODevice::WriteOnly | QFile::Text | QFile::Truncate);
        }
        if (openedFile->isOpen()) {
            openedFile->write(ui->textEdit->document()->toPlainText().toUtf8().data());
            openedFile->close();
        }
        else {
            QMessageBox::critical(this, "Error saving file", "Could not open file to save");
        }
    }
}


void BrainyWindow::saveFileAs()
{
    if (openedFile) {
        openedFile->close();
        openedFile.reset(nullptr);
    }
    saveFile();
}


void BrainyWindow::startRun()
{
    ZombieRuntime* zr = new ZombieRuntime();
    zr->runCode(ui->textEdit->document()->toPlainText().toLatin1().data());
    connect(zr, &ZombieRuntime::hasOutput, this, &BrainyWindow::writeConsole);

    this->runJobs.append(zr);
    /*zombie::ZombieRuntime zrt(ui->textEdit->document()->toPlainText().toLatin1().data());
    zrt.setOnOutput([this] (uint8_t c) {
        //printf("%c", c);
        const char str[] = { char(c), '\0' };
        //fflush(stdout);
        QMetaObject::invokeMethod(this->ui->runConsole, "insertPlainText", Qt::DirectConnection, Q_ARG(QString, str));
    });
    zrt.run();*/
    //this->ui->runConsole->append("output goes here");
}


bool BrainyWindow::changesToSave() const
{
    return changedAnything;
}


void BrainyWindow::writeConsole(const QString& s)
{
    QMetaObject::invokeMethod(this->ui->runConsole, "insertPlainText", Qt::DirectConnection, Q_ARG(QString, s));
}

