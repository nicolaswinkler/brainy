/*
 * This file is part of the brainy brainfuck IDE.
 *
 * Copyright (C) 2018   Nicolas Winkler (nicolas.winkler@gmx.ch)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef EDITORWINDOW_H
#define EDITORWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "syntaxhighlighter.h"
#include <memory>

namespace Ui
{
    class BrainyWindow;
}


class BrainyWindow : public QMainWindow
{
    Q_OBJECT

    bool changedAnything;
    std::unique_ptr<QFile> openedFile;
    QVector<QThread*> runJobs;
public:
    explicit BrainyWindow(QWidget *parent = nullptr);
    ~BrainyWindow();

    void setTextFont(const QFont& font);

    void openFile();
    void saveFile();
    void saveFileAs();

    void startRun();

    bool changesToSave() const;

private slots:
    void writeConsole(const QString& qs);

private:
    std::unique_ptr<Ui::BrainyWindow> ui;
    std::unique_ptr<SyntaxHighlighter> sh;
};

#endif // EDITORWINDOW_H
