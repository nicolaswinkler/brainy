# Brainy

This repository contains the source code for Brainy, a dynamic, fast and configurable IDE for the popular programming language Brainfuck.

Brainy provides an astonishingly clean and intuitive interface to simplify editing and writing program code in the notoriously hard-to-read and hard-to-write Brainfuck language.

## Development

Brainy is currently still being developed.

Expect many more features to come ;)
