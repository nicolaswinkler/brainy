#include "zombie.h"
#include <memory>
#include <cstring>

using zombie::ZombieRuntime;



void ZombieRuntime::run()
{
    int32_t instPointer = 0;
    uint16_t memPointer = 0;

    size_t buflen = 1 << 16;
    auto buffer = std::make_unique<uint8_t[]>(buflen);
    std::memset(buffer.get(), 0, buflen);

    while (instPointer < code.length()) {
        char c = code[instPointer];
        switch (c) {
        case '+':
            buffer[memPointer]++;
            break;
        case '-':
            buffer[memPointer]--;
            break;
        case '>':
            memPointer++;
            break;
        case '<':
            memPointer--;
            break;
        case ',':
            fread(&buffer[memPointer], 1, 1, this->instream);
            break;
        case '.':
            onOutput(buffer[memPointer]);
            //printf("%c", buffer[memPointer]);
            //fflush(stdout);
            break;
        case '[':
        {
            if (!buffer[memPointer]) {
                instPointer++;
                for (int level = 1; level > 0 && instPointer < code.length(); instPointer++) {
                    if (code[instPointer] == '[')
                        level++;
                    else if (code[instPointer] == ']')
                        level--;
                }
                instPointer--;
            }
        }
            break;
        case ']':
        {
            instPointer--;
            for (int level = 1; level > 0 && instPointer >= 0; instPointer--) {
                if (code[instPointer] == ']')
                    level++;
                else if (code[instPointer] == '[')
                    level--;
            }
        }
            break;
        default:
            break;
        }
        instPointer++;
    }
}
