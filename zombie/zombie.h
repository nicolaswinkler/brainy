#ifndef ZOMBIE_H
#define ZOMBIE_H

#include <string>
#include <functional>
#include <cstdio>

namespace zombie
{
    class ZombieRuntime;
}

class zombie::ZombieRuntime
{
    std::string code;
    FILE* instream;
    std::function<void(uint8_t)> onOutput;
public:
    inline ZombieRuntime(const std::string& code) :
        code{ code },
        onOutput{ [] (uint8_t) {} }
    {
    }

    inline void setOnOutput(std::function<void(uint8_t)> callback) {
        onOutput = callback;
    }

    void run();
};

#endif // ZOMBIE_H
