#include "runtime.h"

Runtime::Runtime(void)
{
}


Runtime::~Runtime(void)
{
}


void ZombieRuntime::run(void)
{
    printf("pre-start\n"); fflush(stdout);
    process.start("zombie.exe", QStringList() << "-i");
    printf("post-start\n"); fflush(stdout);
    auto state = process.state();
    //emit hasOutput("output of program");
    if (state == QProcess::Running) {
        process.write(code.c_str(), code.length());
        process.closeWriteChannel();
        printf("waiting for output\n"); fflush(stdout);
        while(true) {
            process.waitForReadyRead(1000);
            auto bytes = process.readAllStandardOutput();
            emit hasOutput(QString::fromLatin1(bytes));
            if (process.state() == QProcess::NotRunning)
                break;
        }
    }
}


int ZombieRuntime::runCode(const std::string& code)
{
    this->code = code;
    this->start();
    return 0;
}


void ZombieRuntime::write(const std::string& code)
{
    process.write(code.c_str(), code.length());
}
