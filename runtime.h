#ifndef RUNTIME_H
#define RUNTIME_H

#include <string>
#include <QProcess>
#include <QThread>

class Runtime : public QThread
{
    Q_OBJECT
public:
    Runtime(void);
    virtual ~Runtime(void);

    virtual int runCode(const std::string& code) = 0;
    virtual int runFile(const std::string& file) = 0;
};


class ZombieRuntime :
        public Runtime
{
    Q_OBJECT

    QProcess process;
    std::string code;
public:
    virtual void run(void) override;
    virtual int runCode(const std::string& code) override;
    virtual void runFile(const std::string& file) override;

signals:
    void hasOutput(const QString& s);
public slots:
    void write(const std::string& s);
};

#endif // RUNTIME_H
