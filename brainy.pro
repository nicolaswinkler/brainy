#
# This file is part of the brainy brainfuck IDE.
#
# Copyright (C) 2018   Nicolas Winkler (nicolas.winkler@gmx.ch)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#

QT       += core gui

win32:RC_ICONS+=Icon.ico
ICON=Icon.icns

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = brainy
TEMPLATE = app

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    runtime.cpp \
    syntaxhighlighter.cpp \
    brainywindow.cpp \
    zombie/zombie.cpp

HEADERS += \
    runtime.h \
    syntaxhighlighter.h \
    brainywindow.h \
    zombie/zombie.h

FORMS += \
    brainywindow.ui
